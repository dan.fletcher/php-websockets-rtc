var socket = null;

function bootstrap() {

      context = canvas.getContext('2d');

  // 適当な図形を描画
  //var c = document.getElementById('mycanvas');
  //var ctx = c.getContext('2d');
  //ctx.globalalpha = 0.3;
  //for(var i=0; i<1000; i++) {
  //  ctx.beginPath();
//    var r = Math.floor(Math.random() * 256);
//    var g = Math.floor(Math.random() * 256);
//    var b = Math.floor(Math.random() * 256);
//    ctx.strokeStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
//    ctx.moveTo(Math.random()*200, Math.random()*200);
//    ctx.lineTo(Math.random()*200, Math.random()*200);
//    ctx.stroke();
//  }

  // Socketの初期化
  socket = new WebSocket('ws://127.0.0.1:9000/caams');
  socket.binaryType = 'arraybuffer';
  socket.onopen = function() {
    send(context);
  }
  socket.onmessage = handleReceive
};

function send(ctx) {
  // RAWデータをそのまま送信
  var data = ctx.getImageData(0, 0, 100, 100).data;
  var byteArray = new Uint8Array(data);
  socket.send(byteArray.buffer);
  setTimeout(send, 500, ctx);
}

function handleReceive(message) {
  // 受信したRAWデータをcanvasに
  var c = resultCanvas = document.getElementById('result');
  var ctx = c.getContext('2d');
  var imageData = ctx.createImageData(100, 100);
  var pixels = imageData.data;

  var buffer = new Uint8Array(message.data);
  for (var i=0; i < pixels.length; i++) {
    pixels[i] = buffer[i];
  }
  ctx.putImageData(imageData, 0, 0);
}
