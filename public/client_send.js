var socket = null;

function bootstrap() {

      context = canvas.getContext('2d');


  // Socketの初期化
  socket = new WebSocket('ws://127.0.0.1:9000/caams');
  socket.binaryType = 'arraybuffer';
  socket.onopen = function() {
    send(context);
  }
  socket.onmessage = handleReceive
};

function send(ctx) {
  // RAWデータをそのまま送信
  var data = ctx.getImageData(0, 0, 100, 100).data;
  var byteArray = new Uint8Array(data);
  socket.send(byteArray.buffer);
  setTimeout(send, 300, ctx);
}

function handleReceive(message) {
  // 受信したRAWデータをcanvasに
  var c = resultCanvas = document.getElementById('result');
  var ctx = c.getContext('2d');
  var imageData = ctx.createImageData(100, 100);
  var pixels = imageData.data;

  var buffer = new Uint8Array(message.data);
  for (var i=0; i < pixels.length; i++) {
    pixels[i] = buffer[i];
  }
  ctx.putImageData(imageData, 0, 0);
}
