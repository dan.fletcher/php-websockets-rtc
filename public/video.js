(function() {
  var canvas = document.getElementById('canvas'),
      context = canvas.getContext('2d'),
      video = document.getElementById('video'),
      vendorUrl = window.URL || window.webkitURL;

  navigator.getMedia = navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia;

  navigator.getMedia({
    video: true,
    audio: false
  }, function(stream) {
      video.src = vendorUrl.createObjectURL(stream);
      video.play();
  }, function(error){
      // An error occured
      // error.code is the error code
  });

  video.addEventListener('play', function(){
    draw(video, context, 100, 100);
  }, false);


  function draw(video, context, width, hight){
    context.drawImage(video, 0, 0, width, hight);
    setTimeout(draw, 10, video, context, width, hight);
  };

})();
