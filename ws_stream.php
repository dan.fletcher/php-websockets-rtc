#!/usr/bin/env php
<?php

require_once('websockets.php');
require_once('users.php');

class caamsServer extends WebSocketServer {
  //protected $maxBufferSize = 1048576; //1MB... overkill for an echo server, but potentially plausible for other applications.


  protected function process ($user, $message) {

	$this->send($user, $message);
  //  print "Data : ".$message."\r\n";

    foreach ($this->users as $currentuser) {
      if ($currentuser != $user)
      {
        print $currentuser->id." : ".$message."\r\n";
        $this->send($currentuser,$message);
      }
    }
  }

  protected function connected ($user) {
    print "User Connected : ".$user->id."\r\n";
  }

  protected function closed ($user) {
    print "User Disconencted : ".$user->id."\r\n";
  }
}

$echo = new caamsServer("localhost","9000");

try {
  $echo->run();
}
catch (Exception $e) {
  $echo->stdout($e->getMessage());
}
